﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewHammerTip : MonoBehaviour
{
    public Vector3 v3Offset;
    public float fDistance = 3.13f;

    public float fForwardDistance = 0.14f;
    public float fLeftDistance = 0.42f;
    public float fBackwardsDistance = 0.14f;
    public float fRightDistance = 0.42f;

    public HammerControl m_HammerControl;
    public Phys m_Phys;

    Vector3 v3Center;
    Vector3 v3PrevPos;

    Vector3 v3Velocity;

    Vector3 v3MousePos;

    public bool bNewContact = true;
    Vector3 v3InitialContactPoint;

    public float fTipFriction = 0.9f;
    public float fForceDampening = 1f;

    public float fMaxOffset = 2f;

    public bool bInContact = false;
    // Use this for initialization
    void Start ()
    {
        v3Offset = transform.forward;
        gameObject.layer = 2;

        v3Center = (v3Offset * fDistance) + this.transform.position;
        v3PrevPos = v3Center;
    }

    public void Check()
    {
        v3MousePos = Input.mousePosition;
        v3MousePos = Camera.main.ScreenToWorldPoint(v3MousePos);

        v3Offset = transform.forward;
        v3Center = (v3Offset * fDistance) + this.transform.position;

        v3Velocity = v3PrevPos - v3Center;

        RaycastHit hitInfo;

        if (Physics.Raycast(v3Center, transform.forward, out hitInfo, (transform.forward * fForwardDistance).magnitude))
        {
            bInContact = true;

            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
            }

            m_HammerControl.SetTargetPoint(v3InitialContactPoint);
        }
        else if (Physics.Raycast(v3Center, transform.forward, out hitInfo, (transform.forward * -fBackwardsDistance).magnitude))
        {
            bInContact = true;

            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
            }

            m_HammerControl.SetTargetPoint(v3InitialContactPoint);
        }
        else if (Physics.Raycast(v3Center, transform.right, out hitInfo, (transform.right * fRightDistance).magnitude))
        {
            bInContact = true;

            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
            }

            m_HammerControl.SetTargetPoint(v3InitialContactPoint);
        }
        else if (Physics.Raycast(v3Center, transform.right * -1, out hitInfo, (transform.right * -fLeftDistance).magnitude))
        {
            bInContact = true;

            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
            }

            m_HammerControl.SetTargetPoint(v3InitialContactPoint);
        }
        else if (Physics.Raycast(v3PrevPos, v3Velocity * -1, out hitInfo, v3Velocity.magnitude))
        {
            /*
            print("We've moved too quickly and hit something");

            bInContact = true;

            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
            }
            float fOffset = Mathf.Cos(Mathf.Deg2Rad * m_HammerControl.fAngle);
            m_HammerControl.SetTargetPoint(hitInfo.point + (hitInfo.normal * 0.65f));
            */
        }
        else
        {
            bInContact = false;
            bNewContact = true;
            m_HammerControl.ClearTargetPoint();
        }


       

        //Debug.DrawRay(v3Center, transform.forward * fForwardDistance, Color.red, 1);
        //Debug.DrawRay(v3Center, transform.forward * -fBackwardsDistance, Color.blue, 1);
        //Debug.DrawRay(v3Center, transform.right * fRightDistance, Color.black, 1);
        //Debug.DrawRay(v3Center, transform.right * -fLeftDistance, Color.green, 1);
        //v3PrevPos = (v3Offset * fDistance) + this.transform.position;
    }

    public bool CheckHit(out RaycastHit v3Impact)
    {
        bool bRet = false;
        v3Offset = transform.forward;
        v3PrevPos = v3Center;
        v3Center = (v3Offset * fDistance) + this.transform.position;
        v3Velocity = v3PrevPos - v3Center;

        Debug.DrawLine(v3PrevPos, v3Center, Color.black, 0.2f);

        if (Physics.Raycast(v3Center, transform.forward, out v3Impact, (transform.forward * fForwardDistance).magnitude))
        {
            bInContact = true;
            bRet = true;
            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
            }
            v3InitialContactPoint = v3Center;
            //m_HammerControl.SetTargetPoint(v3InitialContactPoint, transform.forward, fForwardDistance);
        }
        else if (Physics.Raycast(v3Center, transform.forward, out v3Impact, (transform.forward * -fBackwardsDistance).magnitude))
        {
            bInContact = true;
            bRet = true;
            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
            }
            v3InitialContactPoint = v3Center;
            //m_HammerControl.SetTargetPoint(v3InitialContactPoint, transform.forward * -1, fBackwardsDistance);
        }
        else if (Physics.Raycast(v3Center, transform.right, out v3Impact, (transform.right * fRightDistance).magnitude))
        {
            bInContact = true;
            bRet = true;
            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
            }
            v3InitialContactPoint = v3Center;
            //m_HammerControl.SetTargetPoint(v3InitialContactPoint, transform.right, fRightDistance);
        }
        else if (Physics.Raycast(v3Center, transform.right * -1, out v3Impact, (transform.right * -fLeftDistance).magnitude))
        {
            bInContact = true;
            bRet = true;
            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
            }
            v3InitialContactPoint = v3Center;

            //m_HammerControl.SetTargetPoint(v3InitialContactPoint, transform.right * -1, fLeftDistance);
        }
        else
        {
           
        }


        return bRet;
    }

    public bool CheckDifference(out RaycastHit v3Impact)
    {
        bool bRet = false;

        if (Physics.Raycast(v3PrevPos, v3Velocity * -1, out v3Impact, v3Velocity.magnitude))
        {
            //this movement will take us too far
            bRet = true;
            bInContact = true;
        }

        if (!bInContact)
             bNewContact = true;

        if (bInContact)
        {
            v3MousePos = Input.mousePosition;
            v3MousePos = Camera.main.ScreenToWorldPoint(v3MousePos);

            Vector3 v3Offset = v3MousePos - v3InitialContactPoint;
            v3Offset.x = 0;
            v3Offset = Vector3.ClampMagnitude(v3Offset, fMaxOffset);

            if (!m_Phys.m_bIsGrounded && m_Phys.v3MoveDirection.y < 0)
            {
                m_Phys.v3MoveDirection.y *= 0.5f;
                m_Phys.v3MoveDirection.z *= 0.5f;
            }
            //m_Phys.v3MoveDirection.y = 0;
            if (m_Phys.m_bIsGrounded && ((v3Offset.y * -1 * fForceDampening) < 0))
            {

            }
            else
            {
                m_Phys.v3MoveDirection += v3Offset * -1 * fForceDampening;
            }

       

            m_Phys.v3MoveDirection.z *= fTipFriction;
        }

        return bRet;
    }
	
	// Update is called once per frame
	void Update ()
    {
        Check();
    }
}
