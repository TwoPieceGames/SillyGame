﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndTrigger : MonoBehaviour
{
    private Phys m_Phys;

    public Vector3 v3EndSpeed = new Vector3(0, 3, 0);

    public AudioSource FinalSource;

    private bool bStartedEnd = false;

    public GameObject goCredits;

    public delegate void StartedEnd();
    public static event StartedEnd OnEnd;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Phys>())
        {
            if (!bStartedEnd)
            {
                bStartedEnd = true;
                m_Phys = other.GetComponent<Phys>();
                GetComponent<AudioSource>().Play();
                FinalSource.Stop();
                goCredits.GetComponent<Animator>().Play("End");
                StartCoroutine(StartEndSequence());
                //play credits
            }
        }
    }

    IEnumerator StartEndSequence()
    {
        yield return new WaitForSeconds(43);
        m_Phys.enabled = false;
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene("MainMenu");
    }

    void Pause()
    {
        GetComponent<AudioSource>().Pause();
    }


    void UnPause()
    {
        GetComponent<AudioSource>().UnPause();
    }

    // Use this for initialization
    void Start ()
    {
        PauseMenuHandler.OnPlayerPause += Pause;
        PauseMenuHandler.OnPlayerUnPause += UnPause;
    }

    private void OnDestroy()
    {
        PauseMenuHandler.OnPlayerPause -= Pause;
        PauseMenuHandler.OnPlayerUnPause -= UnPause;
    }

    // Update is called once per frame
    void LateUpdate ()
    {
        if (m_Phys)
            m_Phys.v3MoveDirection = v3EndSpeed;
    }
}
