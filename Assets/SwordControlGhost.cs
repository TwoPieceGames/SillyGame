﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordControlGhost : MonoBehaviour
{
    public GameObject goPointer;

    public GameObject goTarget;
    public Vector3 v3SwordOffset = new Vector3(0, 1, 0);

    private Vector3 v3PointerOffset = new Vector3(1, 0, 0);

    public GameObject pref_Target;

    private Vector3 v3PrevMousePos;
    public Vector3 v3MousePos;
    GameObject goPoint;
    Vector3 v3Offset;
    Vector3 v3T = new Vector3(0, 0, 0);

    public float fAngle;

    Vector3 v3EulerAngles = new Vector3(-90, 0, 0);

    public float fSensitivity = 1.0f;
    private float fXVal;
    private float fYVal;

    public float fDampening = 0.9f;

    public float fRadius = 1;
    public float fInnerRadius = 1.5f;
    public Vector3 v3CenterPoint;
    public Vector3 v3CollisionPoint;

    public float fMouseLead = 1.85f;

    public float m_fMouseCollisionDistance = 0.1f;
    private Vector3 v3Dir;
    private Vector3 v3TargetPoint;


    public bool bCollision = false;
    public HammerTip m_Tip;

    public float fOffset = 1;

    Vector3 v3PrevPos;
    Vector3 v3PrevRotation;

    public float fRayCastHitOFfset = 0.3f;

    public static float MinMax(float x, float max, float min, float dNewMax, float dNewMin)
    {
        if (x > max)
            x = max;
        else if (x < min)
            x = min;

        x = (x - min) * (dNewMax - dNewMin) / (max - min) + dNewMin;
        return x;
    }
    // Use this for initialization
    void Start()
    {
        goPoint = GameObject.Instantiate(pref_Target);
        v3CenterPoint = goTarget.transform.position + v3SwordOffset;
        goPoint.SetActive(false);
        //Cursor.visible = false;

        v3MousePos = Input.mousePosition;
        v3MousePos = Camera.main.ScreenToWorldPoint(v3MousePos);

        v3PrevMousePos = v3MousePos;
    }

    public void Move(Vector3 v3PointOffset)
    {
        //set this so we can use it when updating bad collisions
        v3CollisionPoint = v3PointOffset;

        v3CenterPoint = goTarget.transform.position + v3SwordOffset;
        v3MousePos = v3PointOffset;
        v3MousePos.x = v3CenterPoint.x;
        goPoint.transform.position = v3MousePos;

        v3MousePos *= fSensitivity;

        //Rotates toward the mouse
        fAngle = Mathf.Atan2((v3MousePos.y - transform.position.y), -1 * (v3MousePos.z - transform.position.z)) * Mathf.Rad2Deg - 180;
        transform.eulerAngles = new Vector3(Mathf.Atan2((v3MousePos.y - transform.position.y), -1 * (v3MousePos.z - transform.position.z)) * Mathf.Rad2Deg - 180, 0, 90);

        /*
        v3EulerAngles.y = 0;
        v3EulerAngles.x = 0;
        transform.rotation = Quaternion.Euler(v3EulerAngles);
        */

        v3Offset = v3MousePos - v3CenterPoint;
        v3Offset = Vector3.ClampMagnitude(v3Offset, fRadius);

        this.transform.position = Vector3.Lerp(v3CenterPoint, v3CenterPoint + v3Offset, fDampening);
        this.transform.position -= transform.forward * fMouseLead;

        Vector3 v3Intended = this.transform.position - (transform.forward * fMouseLead);
        v3Intended = v3Intended - v3CenterPoint;
        if (v3Intended.magnitude > fInnerRadius)
        {
            v3Intended = Vector3.ClampMagnitude(v3Intended, fInnerRadius);
        }
        //this.transform.position -= transform.forward * fMouseLead;
        this.transform.position -= v3Intended;

        Debug.DrawRay(v3MousePos, Vector3.up, Color.green, 6);
    }

    public void Revert()
    {
        this.transform.position = v3PrevPos;
        this.transform.rotation = Quaternion.Euler(v3PrevRotation);
    }

    public void SetTargetPoint(Vector3 v3Target)
    {
        v3TargetPoint = v3Target;
        bCollision = true;
    }

    public void SetTargetPoint(Vector3 v3Target, Vector3 v3Line, float fDistance)
    {
        v3TargetPoint = v3Target + (v3Line * fDistance * fOffset);
        bCollision = true;
    }

    public void ClearTargetPoint()
    {
        bCollision = false;
    }

    private bool CollisionTest()
    {
        bool bRet = false;

        RaycastHit hitInfo;

        //draw ray from prev position to new position
        v3MousePos = Input.mousePosition;
        v3MousePos = Camera.main.ScreenToWorldPoint(v3MousePos);

        Vector3 v3Dir = v3PrevMousePos - v3MousePos;

        //Debug.DrawLine(v3PrevMousePos, v3MousePos, Color.blue);
        //Debug.DrawRay(v3PrevMousePos, v3Dir, Color.red, 0.3f);

        if (Physics.Raycast(v3PrevMousePos, v3Dir * -1, out hitInfo, v3Dir.magnitude + m_fMouseCollisionDistance))
        {
            v3TargetPoint = hitInfo.point;
            bRet = true;
        }

        return bRet;
    }

    void Calculate()
    {

        v3CenterPoint = goTarget.transform.position + v3SwordOffset;

        v3TargetPoint.x = v3CenterPoint.x;
        goPoint.transform.position = v3TargetPoint;

        Vector3 v3Dif = v3TargetPoint - v3CenterPoint;

        if (v3Dif.magnitude < fInnerRadius)
        {
            v3TargetPoint = (v3Dif.normalized * fInnerRadius) + v3CenterPoint;
        }

        //Rotates toward the mouse
        fAngle = Mathf.Atan2((v3TargetPoint.y - v3CenterPoint.y), -1 * (v3TargetPoint.z - v3CenterPoint.z)) * Mathf.Rad2Deg - 180;
        transform.eulerAngles = new Vector3(Mathf.Atan2((v3TargetPoint.y - v3CenterPoint.y), -1 * (v3TargetPoint.z - v3CenterPoint.z)) * Mathf.Rad2Deg - 180, 0, 90);

        v3Offset = v3TargetPoint - v3CenterPoint;
        v3Offset = Vector3.ClampMagnitude(v3Offset, fRadius);

        //this.transform.position = Vector3.Lerp(v3CenterPoint, v3CenterPoint + v3Offset, fDampening);
        this.transform.position = v3CenterPoint + v3Offset;
        this.transform.position -= transform.forward * fMouseLead;
    }

    // Update is called once per frame
    void Update()
    {
        v3MousePos = Input.mousePosition;
        v3MousePos = Camera.main.ScreenToWorldPoint(v3MousePos);
        v3TargetPoint = v3MousePos;

        Calculate();

        RaycastHit hitInfo;

        /*
        //check to see if this movement will place us through terrain
        if (m_Tip.CheckHit(out hitInfo))
        {
            //move ourselves away based on hitnormal
            Vector3 v3Dif = hitInfo.point - v3TargetPoint;

            bCollision = true;
            //v3TargetPoint = hitInfo.point + (hitInfo.normal * 0.35f);

            //revert
            Revert();


        }

        if (m_Tip.CheckDifference(out hitInfo))
        {
            Revert();

            float fNormalAngle = Mathf.Atan2(hitInfo.normal.y, -1 * hitInfo.normal.z) * Mathf.Rad2Deg - 180;
            print("Normal Angle: " + fNormalAngle);
            float fOff = Mathf.Sin(Mathf.Deg2Rad * (fAngle - fNormalAngle));
            print(fNormalAngle);
            fOff = Mathf.Abs(fOff);
            fOff = HammerControl.MinMax(fOff, 1, 0, 1, -0.3f);
            print(fOff);
            v3TargetPoint = hitInfo.point + (hitInfo.normal * fRayCastHitOFfset * fOff);

            v3CenterPoint = goTarget.transform.position + v3SwordOffset;

            v3TargetPoint.x = v3CenterPoint.x;
            goPoint.transform.position = v3TargetPoint;

            Vector3 v3Dif = v3TargetPoint - v3CenterPoint;

            if (v3Dif.magnitude < fInnerRadius)
            {
                v3TargetPoint = (v3Dif.normalized * fInnerRadius) + v3CenterPoint;
            }

            //Rotates toward the mouse
            fAngle = Mathf.Atan2((v3TargetPoint.y - transform.position.y), -1 * (v3TargetPoint.z - transform.position.z)) * Mathf.Rad2Deg - 180;
            transform.eulerAngles = new Vector3(Mathf.Atan2((v3TargetPoint.y - transform.position.y), -1 * (v3TargetPoint.z - transform.position.z)) * Mathf.Rad2Deg - 180, 0, 90);

            v3Offset = v3TargetPoint - v3CenterPoint;
            v3Offset = Vector3.ClampMagnitude(v3Offset, fRadius);

            this.transform.position = Vector3.Lerp(v3CenterPoint, v3CenterPoint + v3Offset, fDampening);

            this.transform.position -= transform.forward * fMouseLead;

        }
        */

        goPoint.transform.position = v3TargetPoint;

        v3PrevMousePos = v3TargetPoint;
        v3PrevPos = this.transform.position;
        v3PrevRotation = this.transform.rotation.eulerAngles;
    }
}
