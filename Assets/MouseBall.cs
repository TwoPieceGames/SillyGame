﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseBall : MonoBehaviour
{

    Vector3 v3Offset;
    public float fSensitivity = 0.1f;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        v3Offset = Vector3.zero;
        v3Offset.z -= Input.GetAxis("Mouse X");
        v3Offset.y += Input.GetAxis("Mouse Y");

        this.transform.position += v3Offset * fSensitivity;
	}
}
