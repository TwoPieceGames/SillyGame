﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phys : MonoBehaviour {

    public float fGravity = -2f;

    public Vector3 v3MoveDirection;

    CharacterController controller;

    public bool m_bIsGrounded = false;
    public Vector3 m_v3GroundNormal;
    private bool bWasGrounded = false;
    public float m_fGroundCheckDistance = 0.3f;
    public float m_fFriction = 0.9f;

    Vector3 v3FixedPos;
    Vector3 v3PrevPos;

    Vector3 v3ActiveForce;

    public HammerTip m_Tip;
    Vector3 v3ThisFramePrevPos;

    public void SetForce(Vector3 v3Force)
    {
        v3ActiveForce = v3Force;
    }

    public void Revert()
    {
        this.transform.position = v3PrevPos;
    }


    void ApplyGravity()
    {
        v3MoveDirection.y -= fGravity;
    }

    void ApplyFriction()
    {
        v3MoveDirection.z *= m_fFriction;
    }

	// Use this for initialization
	void Start ()
    {
        controller = GetComponent<CharacterController>();

    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        //compare hit position with where we are
        if (hit.point.y > this.transform.position.y && hit.point.y - this.transform.position.y > 1.45f)
        {

            if (v3MoveDirection.y > 0)
            {
                v3MoveDirection.y += hit.normal.y;
                Debug.DrawRay(hit.point, hit.normal, Color.blue, 1);
            }
                

        }
        else
        {
            if (hit.point.y > this.transform.position.y + 0.8f)
                v3MoveDirection.y += v3ActiveForce.y;

            if (v3MoveDirection.y < 0)
                v3MoveDirection.z *= 0.5f;

            if (hit.point.y < this.transform.position.y)
            {
                if (v3MoveDirection.y < -2f)
                    v3MoveDirection.y = -2f;
            }
        }
    }

    void CheckGroundStatus()
    {
        RaycastHit hitInfo;
#if UNITY_EDITOR
        // helper to visualise the ground check ray in the scene view
        //Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * m_fGroundCheckDistance), Color.red, 60);
#endif
        // 0.1f is a small offset to start the ray from inside the character
        // it is also good to note that the transform position in the sample assets is at the base of the character
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_fGroundCheckDistance))
        {
            m_v3GroundNormal = hitInfo.normal;
            m_bIsGrounded = true;
        }
        else
        {
            m_bIsGrounded = false;
            m_v3GroundNormal = Vector3.up;
        }

        if (v3PrevPos.y - this.transform.position.y < -0.01 && v3MoveDirection.y < 0)
        {
            m_bIsGrounded = true;
        }
        else if (v3PrevPos.y - this.transform.position.y > 0.01 && v3MoveDirection.y > 0)
        {
            if (v3MoveDirection.y > 0)
                v3MoveDirection.y *= 0.05f;
        }
    }

    IEnumerator GetPrevPos()
    {
        yield return new WaitForFixedUpdate();
        v3PrevPos = this.transform.position;
    }

    void HammerClipCheck()
    {
        RaycastHit hitInfo;

        
        if (!m_Tip.bInContact)
        {
            m_Tip.UpdateCenter();
            if (m_Tip.CheckDifference(out hitInfo))
            {
                //our movement caused a tip to pass through
                Debug.DrawRay(m_Tip.v3Center, Vector3.down, Color.red, 1);
                Revert();
                this.transform.position += hitInfo.normal * 0.05f;
                v3MoveDirection.y += (v3MoveDirection.y * -1) + (hitInfo.normal.y * 0f);
                
            }
        }

    }

    public bool MoveCheck(out RaycastHit hitInfo)
    {
        bool bRet = true;
        Vector3 v3PreviousPosition = this.transform.position;

        this.transform.position += (v3MoveDirection * Time.deltaTime) + (new Vector3(0, fGravity, 0) * Time.deltaTime);
        m_Tip.UpdateCenter();
        if (m_Tip.CheckDifference(out hitInfo))
        {
            //our movement caused a tip to pass through
            Debug.DrawRay(m_Tip.v3Center, Vector3.down, Color.cyan, 1);
            bRet = true;
        }
        this.transform.position = v3PreviousPosition;

        return bRet;
    }

    // Update is called once per frame
    void Update ()
    {
        RaycastHit hiInfo;
        CheckGroundStatus();
        if (!m_bIsGrounded)
        {
            ApplyGravity();
            bWasGrounded = false;

        }
        else
        {
            if (!bWasGrounded)
                v3MoveDirection.y = -2f;


            ApplyFriction();

            bWasGrounded = true;
        }

        v3MoveDirection.x = 0;
        StartCoroutine(GetPrevPos());
        v3ThisFramePrevPos = this.transform.position;
        controller.Move(v3MoveDirection * Time.deltaTime);
        FixXPos();
        Debug.DrawRay(this.transform.position, v3MoveDirection * Time.deltaTime, Color.red);
        HammerClipCheck();
    }

    void FixXPos()
    {
        v3FixedPos = this.transform.position;
        v3FixedPos.x = 0;
        this.transform.position = v3FixedPos;
    }
}
