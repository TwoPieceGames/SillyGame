﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTrigger : MonoBehaviour {

    AudioSource m_AudioSource;

    private bool bPlayed = false;
	// Use this for initialization
	void Start () {
        m_AudioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!bPlayed)
        {
            m_AudioSource.Play();
            bPlayed = true;
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
