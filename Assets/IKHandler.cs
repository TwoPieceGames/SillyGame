﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKHandler : MonoBehaviour
{
    public Transform tObj;
    public Transform tTarget;
    public Vector3 v3OffsetLook;
    public Vector3 v3OffsetRHPos;

    public Vector3 v3RHRotationOffset;


    public Vector3 v3OffsetLHPos;
    public Vector3 v3LHRotationOffset;

    Transform tTemp;

    public int iChainLength = 1;
    // Use this for initialization
    protected Animator animator;

    public bool ikActive = false;
    public Transform rightHandObj = null;
    public Transform leftHangObj = null;
    public Transform lookObj = null;

    public float fRightHandOffsetMagnitude = 0.5f;
    public float fLeftHandOffsetMagnitude = -0.4f;

    public Vector3 v3RHOffset;
    public Vector3 v3LHOffset;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    //a callback for calculating IK
    void OnAnimatorIK()
    {
        if (animator)
        {

            //if the IK is active, set the position and rotation directly to the goal. 
            if (ikActive)
            {

                // Set the look target position, if one has been assigned
                if (lookObj != null)
                {
                    animator.SetLookAtWeight(1);
                    animator.SetLookAtPosition(lookObj.position + v3OffsetLook.magnitude * lookObj.transform.forward);
                }

                // Set the right hand target position and rotation, if one has been assigned
                if (rightHandObj != null)
                {
                    v3OffsetRHPos = rightHandObj.transform.forward * fRightHandOffsetMagnitude + v3RHOffset;
                    v3OffsetLHPos = leftHangObj.transform.forward * fLeftHandOffsetMagnitude + v3LHOffset;
                    animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
                    animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
                    animator.SetIKPosition(AvatarIKGoal.RightHand, rightHandObj.position + v3OffsetLHPos);
                    animator.SetIKRotation(AvatarIKGoal.RightHand, rightHandObj.rotation * Quaternion.Euler(v3LHRotationOffset));
                }

                if (leftHangObj != null)
                {
                    animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
                    animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
                    animator.SetIKPosition(AvatarIKGoal.LeftHand, leftHangObj.position + v3OffsetRHPos);
                    animator.SetIKRotation(AvatarIKGoal.LeftHand, leftHangObj.rotation * Quaternion.Euler(v3RHRotationOffset));
                }

            }

            //if the IK is not active, set the position and rotation of the hand and head back to the original position
            else
            {
                animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 0);
                animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 0);
                animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0);
                animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0);
                animator.SetLookAtWeight(0);
            }
        }
    }
}