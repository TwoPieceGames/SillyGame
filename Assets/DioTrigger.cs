﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DioTrigger : MonoBehaviour
{
    private bool bFirstTime = true;
    public AudioSource m_AudioSource;
    public ParticleSystem m_Particles;

    public GameObject goRoadRoller;

    private Phys m_Phys;

    public AudioClip ac3SecondsHanvePassed;
    public AudioClip acRoadRoller;
    public AudioClip acWWRRRRYYYYYYYYYYYYYY;

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.root.GetComponent<Phys>())
        {
            if (bFirstTime)
            {
                m_Phys = other.transform.root.GetComponent<Phys>();
                bFirstTime = false;
                m_AudioSource.Play();
                m_Particles.Play();
                m_Phys.enabled = false;
                StartCoroutine(StartMemes());

            }
        }
    }
    // Use this for initialization
    void Start () {
		
	}
	

    IEnumerator StartMemes()
    {
        yield return new WaitForSeconds(8);
        m_AudioSource.PlayOneShot(ac3SecondsHanvePassed);

        yield return new WaitForSeconds(7);
        //roaod roller
        m_AudioSource.PlayOneShot(acRoadRoller);
        GameObject.Instantiate(goRoadRoller, m_Phys.transform.position, Quaternion.identity);

        yield return new WaitForSeconds(4);
        m_AudioSource.PlayOneShot(acWWRRRRYYYYYYYYYYYYYY);

        yield return new WaitForSeconds(4);

        m_Phys.enabled = true;

    }
}
