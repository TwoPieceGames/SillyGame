﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathTrigger : MonoBehaviour {

    public GameObject goRespawnPoint;
	// Use this for initialization
	void Start () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        other.transform.root.position = goRespawnPoint.transform.position + new Vector3(0, 1, 1);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
