﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenuHandler : MonoBehaviour
{
    public GameObject goMenu;
    public GameObject goOptions;

    public Phys m_Phys;
    public SwordControl m_Sword;
    public SwordControlGhost m_Ghost;
    public HammerTip m_Tip;

    public SaveManager m_SaveManager;


    public Slider MasterAudioSlider;
    public Slider MusicAudioSlider;
    public Slider SFXSlider;
    public Slider VoiceoverSlider;

    public Toggle GachiSounds;

    public AudioMixer m_AudioMixer;

    public delegate void PauseEvent();
    public static event PauseEvent OnPlayerPause;
    public static event PauseEvent OnPlayerUnPause;

    public delegate void ToggleEvent(bool bStatus);
    public static event ToggleEvent OnUseGachiSounds;

    private bool bEnd = false;

    // Use this for initialization
    void Start ()
    {
        goMenu.SetActive(false);
        goOptions.SetActive(false);
        EndTrigger.OnEnd += End;
    }

    private void OnDestroy()
    {
        EndTrigger.OnEnd -= End;
    }

    public void End()
    {
        bEnd = true;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!bEnd)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!goMenu.activeSelf)
                {
                    OpenPauseMenu();
                    if (OnPlayerPause != null)
                    {
                        OnPlayerPause();
                    }
                }
                else
                {
                    if (goOptions.activeSelf)
                    {
                        CloseOptionsMenu();
                    }
                    else
                    {
                        ClosePauseMenu();
                        if (OnPlayerPause != null)
                        {
                            OnPlayerUnPause();
                        }
                    }
                }
            }
        }
		
	}

    public void OpenOptionsMenu()
    {
        goOptions.SetActive(true);

        //set slider values based on actual
    }

    public void CloseOptionsMenu()
    {
        goOptions.SetActive(false);
    }

    public void ApplyOptions()
    {
        SetVolume("Master", float.Parse(MasterAudioSlider.value.ToString()));
        SetVolume("Music", float.Parse(MusicAudioSlider.value.ToString()), -8);
        SetVolume("SFX", float.Parse(SFXSlider.value.ToString()), -11);
        SetVolume("Voiceover", float.Parse(VoiceoverSlider.value.ToString()));

        if (OnUseGachiSounds != null)
        {
            OnUseGachiSounds(GachiSounds.isOn);
        }
    }

    public void OpenPauseMenu()
    {
        goMenu.SetActive(true);

        //disable sword control and phys 
        m_Phys.enabled = false;
        m_Sword.enabled = false;
        m_Ghost.enabled = false;
        m_Tip.enabled = false;
    }

    public void ClosePauseMenu()
    {
        goMenu.SetActive(false);

        //reenable components
        m_Phys.enabled = true;
        m_Sword.enabled = true;
        m_Ghost.enabled = true;
        m_Tip.enabled = true;
    }

    public void Exit()
    {
        m_SaveManager.Save();
        SceneManager.LoadScene("MainMenu");
    }

    public void RevertOptions()
    {

    }

    void SetVolume(string sParam, float fValue, float fMax = 0)
    {
        fValue = SwordControl.MinMax(fValue, 100, 0, fMax, -80);
        m_AudioMixer.SetFloat(sParam, fValue);
    }
}
