﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameData : MonoBehaviour
{
    public bool bLoadedGame;

    public GameObject goContinueButton;
	// Use this for initialization
	void Start ()
    {
        DontDestroyOnLoad(this.gameObject);

        //try to load
        if (File.Exists(Application.dataPath + "SaveFile.txt"))
        {
            goContinueButton.GetComponent<Button>().interactable = true;
        }
        else
        {
            bLoadedGame = false;
        }

        Resolution r = Screen.currentResolution;
        if (r.refreshRate > 70)
            QualitySettings.vSyncCount = 2;
        else
            QualitySettings.vSyncCount = 1;

        Application.targetFrameRate = 60;
    }

    public void LoadGame(bool bLoaded)
    {
        bLoadedGame = bLoaded;
        SceneManager.LoadScene("Scenes/main");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
