﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerTip : MonoBehaviour
{
    public Vector3 v3Offset;
    public float fDistance = 3.13f;

    public float fForwardDistance = 0.14f;
    public float fLeftDistance = 0.42f;
    public float fBackwardsDistance = 0.14f;
    public float fRightDistance = 0.42f;

    public SwordControl m_SwordControl;
    public Phys m_Phys;
    public HammerTipGhost m_Ghost;

    public Vector3 v3Center;
    Vector3 v3PrevPos;
    Vector3 v3PrevPosRendered;

    Vector3 v3Velocity;

    Vector3 v3MousePos;

    Vector3 v3NormalForce;
    public float fNormalForceFactor = 0.4f;

    public bool bNewContact = true;
    Vector3 v3InitialContactPoint;

    public float fTipFriction = 0.9f;
    public float fForceDampening = 1f;
    public float fNormalForce = 1f;

    public float fMaxOffset = 2f;

    public bool bInContact = false;
    private bool bWasInContact = false;

    public float fHorizontalForceIncreaseFactor = 2;

    public AudioSource m_Source;
    public AudioClip m_acSwing;
    public AudioClip m_acHit1;
    public AudioClip m_acHit2;
    public AudioClip m_acHit3;
    public AudioClip m_acSwing2;

    public AudioClip m_acGachi1;
    public AudioClip m_acGachi2;
    public AudioClip m_acGachi3;

    public AudioClip m_acGachiSwing1;
    public AudioClip m_acGachiSwing2;

    private bool bSwooshCooldown = false;

    private bool bUseGachiHammer = false;
    // Use this for initialization
    void Start()
    {
        v3Offset = transform.forward;
        gameObject.layer = 2;

        v3Center = (v3Offset * fDistance) + this.transform.position;
        v3PrevPos = v3Center;

        PauseMenuHandler.OnUseGachiSounds += UseGachi;
    }

    void UseGachi(bool bUse)
    {
        bUseGachiHammer = bUse;
    }

    public void UpdateCenter()
    {
        v3Center = (v3Offset * fDistance) + this.transform.position;
    }

    private void OnDestroy()
    {
        PauseMenuHandler.OnUseGachiSounds -= UseGachi;
    }

    public void Check()
    {
        v3MousePos = Input.mousePosition;
        v3MousePos = Camera.main.ScreenToWorldPoint(v3MousePos);

        v3Offset = transform.forward;
        v3Center = (v3Offset * fDistance) + this.transform.position;

        v3Velocity = v3PrevPos - v3Center;

        RaycastHit hitInfo;

        if (Physics.Raycast(v3Center, transform.forward, out hitInfo, (transform.forward * fForwardDistance).magnitude))
        {
            bInContact = true;

            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
            }

            m_SwordControl.SetTargetPoint(v3InitialContactPoint, hitInfo.normal, 1);
        }
        else if (Physics.Raycast(v3Center, transform.forward, out hitInfo, (transform.forward * -fBackwardsDistance).magnitude))
        {
            bInContact = true;

            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
            }

            m_SwordControl.SetTargetPoint(v3InitialContactPoint, hitInfo.normal, 1);
        }
        else if (Physics.Raycast(v3Center, transform.right, out hitInfo, (transform.right * fRightDistance).magnitude))
        {
            bInContact = true;

            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
            }

            m_SwordControl.SetTargetPoint(v3InitialContactPoint, hitInfo.normal, 1);
        }
        else if (Physics.Raycast(v3Center, transform.right * -1, out hitInfo, (transform.right * -fLeftDistance).magnitude))
        {
            bInContact = true;

            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
            }

            m_SwordControl.SetTargetPoint(v3InitialContactPoint, hitInfo.normal, 1);
        }
        else if (Physics.Raycast(v3PrevPos, v3Velocity * -1, out hitInfo, v3Velocity.magnitude))
        {
            /*
            print("We've moved too quickly and hit something");

            bInContact = true;

            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
            }
            float fOffset = Mathf.Cos(Mathf.Deg2Rad * m_HammerControl.fAngle);
            m_HammerControl.SetTargetPoint(hitInfo.point + (hitInfo.normal * 0.65f));
            */
        }
        else
        {
            bInContact = false;
            bNewContact = true;
            m_SwordControl.ClearTargetPoint();
        }




        //Debug.DrawRay(v3Center, transform.forward * fForwardDistance, Color.red, 1);
        //Debug.DrawRay(v3Center, transform.forward * -fBackwardsDistance, Color.blue, 1);
        //Debug.DrawRay(v3Center, transform.right * fRightDistance, Color.black, 1);
        //Debug.DrawRay(v3Center, transform.right * -fLeftDistance, Color.green, 1);
        //v3PrevPos = (v3Offset * fDistance) + this.transform.position;
    }


    public bool CheckHitNoMove(out RaycastHit v3Impact)
    {
        v3NormalForce = Vector3.zero;
        bool bRet = false;
        v3Offset = transform.forward;
        v3Center = (v3Offset * fDistance) + this.transform.position;
        v3Velocity = v3PrevPos - v3Center;

        //Debug.DrawRay(v3Center, (transform.forward * fForwardDistance), Color.blue, 0.2f);
        //Debug.DrawRay(v3Center, (transform.forward * -fBackwardsDistance), Color.blue, 0.2f);
        Debug.DrawRay(v3Center, (transform.right * fRightDistance), Color.red);
        Debug.DrawRay(v3Center, (transform.right * -fLeftDistance), Color.green);

        if (Physics.Raycast(v3Center, transform.forward, out v3Impact, (transform.forward * fForwardDistance).magnitude))
        {
            bRet = true;
        }
        else if (Physics.Raycast(v3Center, transform.forward, out v3Impact, (transform.forward * -fBackwardsDistance).magnitude))
        {
            bRet = true;
        }
        else if (Physics.Raycast(v3Center, transform.right, out v3Impact, (transform.right * fRightDistance).magnitude))
        {
            bRet = true;
        }
        else if (Physics.Raycast(v3Center, transform.right * -1, out v3Impact, (transform.right * -fLeftDistance).magnitude))
        {
            bRet = true;
        }

        //Debug.DrawRay(v3PrevPosRendered, Vector3.up * 2, Color.cyan);


        return bRet;
    }

    public bool CheckHit(out RaycastHit v3Impact)
    {
        bool bRet = false;
        v3Offset = transform.forward;
        v3Center = (v3Offset * fDistance) + this.transform.position;
        v3Velocity = v3PrevPos - v3Center;

        if (Physics.Raycast(v3Center, transform.forward, out v3Impact, (transform.forward * fForwardDistance).magnitude))
        {
            bInContact = true;
            bRet = true;
            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
                m_SwordControl.SetTargetPoint(v3Center, v3Impact.normal, fRightDistance);
            }

            v3NormalForce = transform.forward * fNormalForceFactor;
            v3InitialContactPoint = v3Center;
        }
        else if (Physics.Raycast(v3Center, transform.forward * -1, out v3Impact, (transform.forward * -fBackwardsDistance).magnitude))
        {
            bInContact = true;
            bRet = true;
            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
                m_SwordControl.SetTargetPoint(v3Center, v3Impact.normal, fRightDistance);
            }
            v3NormalForce = transform.forward * -1 * fNormalForceFactor;
            v3InitialContactPoint = v3Center;
        }
        else if (Physics.Raycast(v3Center, transform.right, out v3Impact, (transform.right * fRightDistance).magnitude))
        {
            bInContact = true;
            bRet = true;
            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
                m_SwordControl.SetTargetPoint(v3Center, v3Impact.normal, fRightDistance);
            }

            v3InitialContactPoint = v3Center;
            
        }
        else if (Physics.Raycast(v3Center, transform.right * -1, out v3Impact, (transform.right * -fLeftDistance).magnitude))
        {
            bInContact = true;
            bRet = true;
            if (bNewContact)
            {
                bNewContact = false;
                v3InitialContactPoint = v3MousePos;
                m_SwordControl.SetTargetPoint(v3Center, v3Impact.normal, fRightDistance);
            }

            v3InitialContactPoint = v3Center;
        }
        else
        {
            m_SwordControl.ClearTargetPoint();
            bInContact = false;
        }


        return bRet;
    }
    public void SetContactPoint(Vector3 vec)
    {
        v3InitialContactPoint = vec;
    }

    private Vector3 ApplyNormalForce()
    {
        float fAng = Mathf.Atan2(v3InitialContactPoint.y - this.transform.position.y, -1 * (v3InitialContactPoint.z - this.transform.position.z)) * Mathf.Rad2Deg - 180;
        Vector3 v3Ang = Vector3.zero;
        v3Ang.y = -Mathf.Sin(Mathf.Deg2Rad * fAng);
        v3Ang.z = -Mathf.Cos(Mathf.Deg2Rad * fAng);
        v3Ang.Normalize();
        v3Ang *= fNormalForce;
        print(fAng);

        return v3Ang;
    }

    public void ApplyForce()
    {
        if (m_SwordControl.bCollision)
        {
            v3MousePos = Input.mousePosition;
            v3MousePos = Camera.main.ScreenToWorldPoint(v3MousePos);

            Vector3 l_v3Offset = m_Ghost.v3Center - v3InitialContactPoint;
            l_v3Offset.x = 0;
            l_v3Offset = Vector3.ClampMagnitude(l_v3Offset, fMaxOffset);

            if (!m_Phys.m_bIsGrounded && m_Phys.v3MoveDirection.y < 0)
            {
                m_Phys.v3MoveDirection.y *= 0.9f;
                m_Phys.v3MoveDirection.z *= 0.9f;
            }

            //l_v3Offset += m_SwordControl.GetVectorAngle() * 1f;

            //m_Phys.v3MoveDirection.y = 0;
            if (m_Phys.m_bIsGrounded && ((l_v3Offset.y * -1 * fForceDampening) < 0))
            {

            }
            else
            {
                l_v3Offset.z *= fHorizontalForceIncreaseFactor;
                //l_v3Offset += ApplyNormalForce();
                float fDist = (v3Center - m_SwordControl.v3CenterPoint).magnitude;
                print(fDist);
                fNormalForceFactor *= fDist / 3.0f;
                l_v3Offset += v3NormalForce * fNormalForceFactor;
                m_Phys.v3MoveDirection += l_v3Offset * -1 * fForceDampening;
            }
           


            m_Phys.SetForce(l_v3Offset);
            m_Phys.v3MoveDirection.z *= fTipFriction;
        }
        else
        {
            m_Phys.SetForce(Vector3.zero);
        }
    }

    public bool CheckDifference(out RaycastHit v3Impact)
    {
        bool bRet = false;
        v3Velocity = v3PrevPosRendered - v3Center;

        if (Physics.Raycast(v3PrevPosRendered, v3Velocity * -1, out v3Impact, v3Velocity.magnitude))
        {
            //this movement will take us too far
            bRet = true;

            bInContact = true;
            m_SwordControl.bCollision = true;
            v3InitialContactPoint = v3Impact.point + (v3Impact.normal * 0.05f);
            
            Debug.DrawLine(v3PrevPosRendered, v3Center, Color.black, 0.2f);
        }
        else
        {
            
        }

        if (!m_SwordControl.bCollision)
            bNewContact = true;


        return bRet;
    }

    IEnumerator SwooshCooldown(float fTime)
    {
        yield return new WaitForSeconds(fTime);
        bSwooshCooldown = false;
    }

    // Update is called once per frame
    void Update()
    {
        v3NormalForce = Vector3.zero;
        //Check();
        StartCoroutine(UpdateLastPosition());

        if (m_SwordControl.bCollision && !bWasInContact)
        {
            if (v3Velocity.magnitude > 0.2f)
            {
                if (!bUseGachiHammer)
                {
                    int iRand = Random.Range(0, 3);
                    if (iRand == 0)
                    {
                        m_Source.PlayOneShot(m_acHit1, Random.Range(0.7f, 1.1f));
                        m_Source.pitch = Random.Range(0.7f, 1.1f);
                    }
                    else if (iRand == 1)
                    {
                        m_Source.PlayOneShot(m_acHit2, Random.Range(0.7f, 1.1f));
                        m_Source.pitch = Random.Range(0.7f, 1.1f);
                    }
                    else if (iRand == 2)
                    {
                        m_Source.PlayOneShot(m_acHit3, Random.Range(0.7f, 1.1f));
                        m_Source.pitch = Random.Range(0.7f, 1.1f);
                    }
                }
                else
                {
                    int iRand = Random.Range(0, 3);
                    if (iRand == 0)
                    {
                        m_Source.PlayOneShot(m_acGachi1, Random.Range(0.7f, 1.1f));
                        m_Source.pitch = Random.Range(0.7f, 1.1f);
                    }
                    else if (iRand == 1)
                    {
                        m_Source.PlayOneShot(m_acGachi2, Random.Range(0.7f, 1.1f));
                        m_Source.pitch = Random.Range(0.7f, 1.1f);
                    }
                    else if (iRand == 2)
                    {
                        m_Source.PlayOneShot(m_acGachi3, Random.Range(0.7f, 1.1f));
                        m_Source.pitch = Random.Range(0.7f, 1.1f);
                    }
                }
            }
            

        }

        if (v3Velocity.magnitude > 0.7f)
        {
            if (bUseGachiHammer)
            {
                if (!bSwooshCooldown)
                {
                    bSwooshCooldown = true;
                    StartCoroutine(SwooshCooldown(0.5f));
                    m_Source.PlayOneShot(m_acSwing, Random.Range(0.7f, 1.1f));
                    m_Source.pitch = Random.Range(0.7f, 1.1f);
                }
            }
            else
            {
                if (!bSwooshCooldown)
                {
                    bSwooshCooldown = true;
                    StartCoroutine(SwooshCooldown(0.5f));
                    m_Source.PlayOneShot(m_acSwing, Random.Range(0.7f, 1.1f));
                    m_Source.pitch = Random.Range(0.7f, 1.1f);
                }
            }


        }

        bWasInContact = m_SwordControl.bCollision;
    }

    IEnumerator UpdateLastPosition()
    {
        yield return null;
        v3PrevPos = v3Center;
        v3PrevPosRendered = v3Center;
      
    }

    public void UpdateHammerPositions()
    {
        v3Offset = transform.forward;
        v3Center = (v3Offset * fDistance) + this.transform.position;
    }
}
