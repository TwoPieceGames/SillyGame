﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public GameObject goTarget;
    public float fDampening;

    public float fDistance;

    Vector3 v3Pos;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        v3Pos = goTarget.transform.position;
        v3Pos.x = fDistance;
        this.transform.position = Vector3.Lerp(this.transform.position, v3Pos, fDampening);
        
	}
}
