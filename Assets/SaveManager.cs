﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    [Serializable]
    public class SavePoint
    {
        public Vector3 v3Position;
        public float fPlayTime;

        public SavePoint(Vector3 v3Pos, float fTime)
        {
            v3Position = v3Pos;
            fPlayTime = fTime;
        }
    }

    public CheckPoint[] m_CheckPoints;
    public CheckPoint m_MostRecentCheckpoint;
    public GameObject m_Camera;
    private float m_fPlayTime;
    // Use this for initialization
    void Start ()
    {
        GameObject goData = GameObject.Find("GameData");
        if (goData)
        {
            GameData _Data = goData.GetComponent<GameData>();
            if (_Data.bLoadedGame)
            {
                Load();
            }
        }


    }
	
	// Update is called once per frame
	void Update () {
        m_fPlayTime += Time.deltaTime;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<CheckPoint>())
        {
            m_MostRecentCheckpoint = other.gameObject.GetComponent<CheckPoint>();
        }
    }

    public void Save()
    {
        //loop through checkpoints, find the first one that we were both above and to the right of
        int iSaveIndex = 0;
        for (int i = 0; i < m_CheckPoints.Length; i++)
        {
            if (this.transform.position.y > m_CheckPoints[i].transform.position.y)
            {
                if (this.transform.position.z < m_CheckPoints[i].transform.position.z)
                {
                    iSaveIndex = i;
                }
                else
                {
                    //break, use previous
                    iSaveIndex = i - 1;
                    break;
                }
            }
            else
            {
                //break, use previous checkpoint
                iSaveIndex = i - 1;
                break;
            }
        }

        if (iSaveIndex < 0)
            iSaveIndex = 0;

        CheckPoint SaveCheckPoint = m_CheckPoints[iSaveIndex];

        SavePoint sp = new SavePoint(SaveCheckPoint.transform.position, m_fPlayTime);
        string Json = JsonUtility.ToJson(sp);
        StreamWriter writer = new StreamWriter(Application.dataPath + "SaveFile.txt", false);
        writer.Write(Json);
        writer.Close();

    }

    public void Load()
    {
        StreamReader reader = new StreamReader(Application.dataPath + "SaveFile.txt");
        SavePoint sp = JsonUtility.FromJson<SavePoint>(reader.ReadToEnd());

        this.transform.position = sp.v3Position + Vector3.up;
        m_Camera.transform.position = sp.v3Position + Vector3.up;
        m_fPlayTime = sp.fPlayTime;
    }
}
