﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyAudioSource : MonoBehaviour
{
    public float fMaxRange = 50;
    public AudioSource m_AudioSource;
    public GameObject goTarget;

    private bool bFirstTouch = true;

    private float fDist;
	// Use this for initialization
	void Start ()
    {
        PauseMenuHandler.OnPlayerPause += Pause;
        PauseMenuHandler.OnPlayerUnPause += UnPause;
    }

    void Pause()
    {
        m_AudioSource.Pause();
    }

    void UnPause()
    {
        m_AudioSource.UnPause();
    }

    private void OnDestroy()
    {
        PauseMenuHandler.OnPlayerPause -= Pause;
        PauseMenuHandler.OnPlayerUnPause -= UnPause;
    }

    // Update is called once per frame
    void Update ()
    {
        fDist = (this.transform.position - goTarget.transform.position).magnitude;
        if (fDist < fMaxRange)
        {
            if (bFirstTouch)
            {
                m_AudioSource.Play();
                bFirstTouch = false;
            }
            float fVol = 1f - (fDist / fMaxRange);
            m_AudioSource.volume = SwordControl.MinMax(fVol, 1, 0, 0.9f, 0);
        }
        else
        {
            m_AudioSource.volume = 0;
        }
    }
}
