﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParralaxBackground : MonoBehaviour
{
    public GameObject goCamera;

    public float fReverseFactor = 0.1f;

    Vector3 v3PrevPosition;
    Vector3 v3Position;
	// Use this for initialization
	void Start ()
    {
        v3Position = goCamera.transform.position;
        v3PrevPosition = v3Position;
    }
	
	// Update is called once per frame
	void LateUpdate ()
    {
        v3Position = goCamera.transform.position;

        Vector3 v3Dif = v3Position - v3PrevPosition;
        this.transform.position -= v3Dif * fReverseFactor;

        v3PrevPosition = v3Position;

    }
}
